import {Head} from "./head";
import localFont from "next/font/local";
import styles from "@/styles/auth.module.scss";
import Image from 'next/image'
import LimoIcon from '../public/LIMO.svg'
import React from "react";
import {AuthProvider} from "@/components/AuthProvider/auth-context";
import {Link} from "@nextui-org/link";
import arrowRight from "@/public/arrow-right.svg";


const YekanBakh = localFont({
    src: './YekanBakh-VF.ttf',
    variable: '--yekan',
})
export default function AuthLayout({
                                       children,
                                   }: {
    children: React.ReactNode;
}) {


    return (

                <div className={` ${YekanBakh.variable}`}>
                    <Head/>

                    <main className={styles.login_page_main}>
                        <div className={styles.login_page}>

                            <div className={styles.login_section}>

                                <Image
                                    color='primary'
                                    width={200}

                                    src={LimoIcon}
                                    alt="NextUI hero Image"
                                />


                                {children}
                            </div>
                        </div>
                    </main>
                </div>


    );
}
