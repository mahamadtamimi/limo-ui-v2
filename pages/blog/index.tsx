// import Breadcrumb from "../../../components/Breadcrumb/Breadcrumb";
// import ChosenMonth from "../../../components/ChosenMonth/ChosenMonth";


import Breadcrumb from "@/components/Global/Breadcrumb";

export default function page({params}: { params: { slug: string } }) {

    const breadCrumb = [
        ['بلاگ', 'blog'],

    ]

    return <main >

        <Breadcrumb data={breadCrumb}/>
        {/*<ChosenMonth />*/}
    </main>
}