import styles from '@/styles/dashboard.module.scss'
import {Link} from "@nextui-org/link";
import Image from "next/image";

import icon from '@/public/folder-2.svg'
import play from '@/public/vuesax-bulk-play-circle.svg'
import DashboardLayout from "@/layouts/dashboard";
import SubCartSummery from "@/components/Dashboard/SubCartSummery";

export default function main() {
    return <DashboardLayout>
        <div className={styles.main_dashboard}>
            <div className={styles.main_dashboard_sec_one}>
                <SubCartSummery/>
                <div className={styles.main_dashboard_ticket}>
                    <div className={styles.main_dashboard_ticket_head}>
                        <p>تیکت‌ها</p>
                        <Link href='/'>
                            مشاهده همه
                        </Link>
                    </div>
                    <div>
                        <div>
                        <span>
                            مشکل در دانلود
                        </span>
                            <span>تیکت باز   </span>


                            <div>
                            <span>
                                <Image src={icon} alt=''/>

                                پشتیبانی فنی
                            </span>
                                <span>
                                آخرین بروزرسانی:
                                ۱۴۰۲/۰۵/۲۷
                            </span>
                            </div>

                        </div>
                        <div>
                        <span>
                            مشکل در دانلود
                        </span>
                            <span>تیکت باز   </span>


                            <div>
                            <span>
                                <Image src={icon} alt=''/>

                                پشتیبانی فنی
                            </span>
                                <span>
                                آخرین بروزرسانی:
                                ۱۴۰۲/۰۵/۲۷
                            </span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.main_dashboard_sec_two}>
                <div>
                    <div>
                        <Image src={play} alt=''/>
                        <span>
                        1
                    </span>
                        <p>
                            لیست تماشای عمومی
                        </p>
                        <button>
                            مشاهده همه
                        </button>
                    </div>


                </div>
                <div>4</div>
                <div>5</div>
            </div>

        </div>
    </DashboardLayout>

}