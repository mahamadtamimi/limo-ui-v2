'use client'
import DashboardLayout from "@/layouts/dashboard";
import styles from "@/styles/subsCart.module.scss";
import {Link} from "@nextui-org/link";
import Image from "next/image";
import {useEffect, useState} from "react";
import SubCart from "@/components/Dashboard/SubCart";
// import icon from "@/public/folder-2.svg";
// import play from "@/public/vuesax-bulk-play-circle.svg";

export default function buySub() {

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [data, setData] = useState()
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/plans`).then(res => res.json()).then(data => {
            setData(data)
        })
    }, []);




    return <DashboardLayout>
        <main className={styles.main_sub_page}>
            <h3 className={styles.main_sub_title}>خرید اشتراک</h3>

            <p className={styles.main_sub_sub_title}>دسترسی به هزاران فیلم و سریال با خرید اشتراک لیمو مووی</p>

            <div className={styles.sub_cart_sec}>
                {
                    // @ts-ignore
                    data && data.map(item =>
                    <SubCart key={`plan-${item.id}`} data={item}/>
                )}
            </div>
        </main>
    </DashboardLayout>


}