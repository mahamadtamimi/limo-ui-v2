import DefaultLayout from "@/layouts/default";
import MainSlider from "@/components/Home/MainSliderServerComponent/MainSlider";
import NewSerials from "@/components/Home/NewSerials/NewSerials";
import NewMovies from "@/components/Home/NewMovies/NewMovies";
import TrailerSlider from "@/components/Home/TrailerSlider/TrailerSlider";
import SortInIMDB from "@/components/Home/SortInIMDB/SortInIMDB";
import Banner from "@/components/Home/Banner/Banner";
import Genres from "@/components/Home/Genres/Genres";
import MostVisited from "@/components/Home/MostVisited/MostVisited";
import Country from "@/components/Home/Country/Country";


export default function IndexPage() {
	return (
		<DefaultLayout>

			<MainSlider/>
			<NewSerials/>
			<NewMovies/>
			<TrailerSlider/>
			<SortInIMDB/>
			<Banner/>
			<Genres/>
			<MostVisited/>

			<Country/>

			{/*<Blog/>*/}


		</DefaultLayout>
	);
}
