import Image from "next/image";
import massage from '@/public/Movir-vuesax-linear-message-text.svg'
import avatar from '@/public/Movir-Frame 48097174.svg'
import like from '@/public/Movir-vuesax-linear-vuesax-linear-like.svg'
import dislike from '@/public/Movir-vuesax-linear-dislike.svg'
import undo from '@/public/Movir-vuesax-linear-vuesax-linear-undo.svg'
import styles from '@/styles/comments.module.scss';
import {Textarea} from "@nextui-org/input";
import {Button} from "@nextui-org/react";
import {useEffect, useState} from "react";
import {useRouter} from "next/router";
import * as shamsi from 'shamsi-date-converter';
import {toast} from "react-toastify";
import {json} from "node:stream/consumers";
import Link from "next/link";


export default function Comments() {
    const router = useRouter();
    // console.log(router)

    const [user, setUser] = useState()
    const [token, setToken] = useState()

    const [comments, setComments] = useState()
    const [commentsLoader, setCommentsLoader] = useState(true)

    const [formData, setFormData] = useState({
        value: '',
        validate: false,
        error: 'tst error'
    })

    useEffect(() => {
        if (router.query.slug?.length) {
            fetch(`${process.env.API_PATH}/api/v1/get-comments`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({mediaSlug: router.query.slug}),
            })
                .then(res => res.json())
                .then(data => {
                    setCommentsLoader(false)
                    setComments(data)
                    // console.log(data)
                })
        }

    }, [router]);

    useEffect(() => {
        // @ts-ignore
        const localUser = JSON.parse(localStorage.getItem('user'))
        const token = localStorage.getItem('token')
        setUser(user)
        // @ts-ignore
        setToken(token)
    }, []);

    function submitComments() {
        fetch(`${process.env.API_PATH}/api/v1/comments/create-comment`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
            body: JSON.stringify({mediaSlug: router.query.slug, body: 'hllo word'}),

        }).then(res => res.json()).then(data => {
            // console.log(data)
        })
    }


    // console.log(user)
    // console.log(token)

    function setLike(e:any) {
        // setCommentsLoader(true)
        toast.loading('لطفا منتظر باشید !', {toastId: "registerToastId"})

        const role = e.target.getAttribute('data-role')
        const id = e.target.getAttribute('data-id')


        fetch(`${process.env.API_PATH}/api/v1/comments/like`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
            body: JSON.stringify({
                mood: role,
                comment_id: id,

            })
        })
            .then(res => {
                res.status === 401 && toast.error('لطفا وارد شوید!')
                res.status === 404 && toast.error('درخواست شما نامعتبر است !')
                res.json()
            })
            .then(data => {

                toast.dismiss('registerToastId')
                console.log(data)


            })


        console.log('test')
    }


    const CommentHead = () => {


        return <>

            <div className={styles.comments_head}>
                <Image src={massage} alt='' width={40} height={40}/>
                <h3 className={styles.comments_title}>
                    نظرات کاربران
                </h3>
            </div>

        {token ?
            <div>

                <Textarea
                    isRequired
                    label="نظر شما"
                    labelPlacement="outside"
                    placeholder={'نظر خود را اینجا بنویسید.'}
                    // maxRows={90}
                    height={500}
                    size={'lg'}
                    classNames={{description : 'description_Box'}}
                />
                {/*<Textarea*/}
                {/*    className={'mb-4 h-80'}*/}
                {/*    value={'test'}*/}
                {/*></Textarea>*/}

                <div className={'flex justify-end mt-3 mb-3'}>
                    <Button className={' btn_primary text-black'} onClick={() => submitComments()}>
                        ثبت
                    </Button>
                </div>
            </div>
            :
            <div className={`${styles.comments_alarm_box} mt-4 mb-4`}>
                برای ثبت نظر خود باید

                <Button as={Link} className={'btn_2x btn_primary_outline mr-2 ml-2'} href={'auth/signup'}>
                    ثبت نام
                </Button>
                یا
                <Button as={Link} className={'btn_2x btn_primary_outline  mr-2 ml-2'} href={'auth/login'}>
                    لاگین
                </Button>
                کنید
            </div>


        }


        </>

    }



    return <div className={styles.comments_sec}>

        <CommentHead/>

        <div>
            {
                // @ts-ignore
                !commentsLoader && comments.map((comment, index) => {
                return <div key={comment.id} className={styles.comments_cart}>
                    <Image src={avatar} alt='' width={88} height={88}/>
                    <div className={styles.comments_body_seec}>
                        <div className={styles.comments_cart_head}>
                            <h4 className={styles.comments_title}>فیلماز اعظم</h4>
                            <span>
                              <button className={styles.span_btn}>
                                  گزارش اسپویل
                            </button>
                            <span className={styles.comments_date}>
                                {shamsi.gregorianToJalali(comment.created_at).join('/')}
                            </span>

                        </span>

                        </div>
                        <p className={styles.comments_pragh}>
                            {comment.body}
                        </p>
                        <div className={styles.comments_footer}>
                            <button data-role={'like'} data-id={comment.id} onClick={(e) => setLike(e)}>
                                {comment.like}
                                <Image data-role={'disLike'} data-id={comment.id} src={like} alt='' width={24}
                                       height={24}/>
                            </button>
                            <button data-role={'disLike'} data-id={comment.id} onClick={(e) => setLike(e)}>
                                {comment.dis_like}
                                <Image data-role={'disLike'} data-id={comment.id} src={dislike} alt='' width={24}
                                       height={24}/>
                            </button>


                        </div>
                    </div>
                </div>
            })}





        </div>

    </div>

}
