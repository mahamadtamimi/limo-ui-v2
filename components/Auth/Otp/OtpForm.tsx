import {Link} from "@nextui-org/link";
import styles from "@/styles/auth.module.scss";
import Image from "next/image";
import arrowRight from "@/public/arrow-right.svg";
import {toast, ToastContainer} from "react-toastify";
import {Button, Input} from "@nextui-org/react";
import React, {useEffect, useLayoutEffect, useState} from "react";
import {useRouter} from "next/router";

import {initialData} from "./initialData";
import 'react-toastify/dist/ReactToastify.css';
import success = toast.success;
import {useDispatch} from "react-redux";


export default function OtpForm() {

    const dispatch = useDispatch();

    const initialsData: any = initialData
    const router = useRouter()
    const [inLoad, setInLoad] = useState(false)
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [formData, setFormData] = useState(initialsData)


    const [user, setUser] = useState(null)




    const [timer, setTimer] = useState(300)
    const [start, setStart] = useState(true)

    const [getNewCode, setGetNewCode] = useState(false)

    async function handleSubmit(event: any) {
        event.preventDefault()
        setInLoad(true)
        toast.loading('لطفا منتظر باشید !', {toastId: "registerToastId"})
        const formDataCollection = new FormData(event.currentTarget)
        const code = formDataCollection.get('code')

        try {
            const data = await fetch('http://127.0.0.1:8000/api/validate-code', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({user: user, code: code}),
            })
            if (data.status == 200) {

                const result = await data.json()


                console.log(result)

                if (result.success) {
                    toast.dismiss('registerToastId')
                    toast.success('ورود موفقیت امیز')


                    dispatch({
                        type: 'SET_TOKEN',
                        payload: {
                            token: result.token,

                        }
                    })

                    router.push('/auth/dashboard/main')
                } else {
                    toast.dismiss('registerToastId')
                    toast.error('کد نامعتبر !')

                }
            }


        } catch (e) {
            toast.dismiss('registerToastId')
            toast.error('خطای سرور')
        }


        setInLoad(false)
        // console.log(data)
        setInLoad(false)
    }

    async function sendCode() {

        setInLoad(true)

        toast.loading('لطفا منتظر باشید !', {toastId: "registerToastId"})

        try {
            const response = await fetch('http://127.0.0.1:8000/api/get-code', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({user: user}),
            })

            if (response.ok) {
                const result = await response.json()
                setGetNewCode(false)
                setTimer(result.time_validate ? result.time_validate : 300)
                toast.dismiss('registerToastId')
                toast.success('کد جدید با موفقیت ارسال شد .')
                setInLoad(false)
                setStart(false)
                console.log(result)
                return result;
            } else {
                toast.dismiss('registerToastId')
                toast.error('خطای سرور !')
            }
        } catch (e) {

        }

    }


    useEffect(() => {
        const getUserFromLocal = localStorage.getItem('user');
        // @ts-ignore
        setUser(getUserFromLocal)
    }, [user]);


    useEffect(() => {

        if (user && start) {
            sendCode()
            setStart(false)
            setTimer(300)
        }

    }, [start, user, sendCode]);


    useEffect(() => {
        if (timer === 0) {
            setGetNewCode(true)
            setTimer(300)

        }

        if (!getNewCode) {
            const interval = setInterval(() => {
                setTimer(timer - 1)
            }, 1000);
            return () => clearInterval(interval);

        }


    }, [timer, getNewCode]);


    function checkFormData(e: any) {
        // console.log(e.target)


        const code = e.target.value
        const valid = code.length == 6;
        if (code.length > 6) return
        const updateDate: any = {
            ...formData,
            code: {
                ...formData.code,
                value: e.target.value,
                validate: valid
            }
        }

        setFormData(updateDate)


    }


    return <>
        <Link className={styles.back_link} onClick={() => router.back()}>
            <Image src={arrowRight} alt={''} width={20} />
            تغییر شماره
        </Link>
        <div>
            {/*<button onClick={notify}>Notify!</button>*/}
            <ToastContainer

                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={true}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
        </div>

        <p className={styles.login_section_prag}>رمز یک بار مصرف</p>
        <div className={styles.login_main_sec}>
            <form onSubmit={handleSubmit}>

                <div className={`${styles.inputSection}`}>
                    <Input
                        name={'code'}
                        value={formData.code.value}
                        variant="bordered"
                        isInvalid={!formData.code.validate}
                        color={!formData.code.validate ? "danger" : "success"}
                        errorMessage={!formData.code.validate && formData.code.error}

                        onChange={(e) => checkFormData(e)}
                        type="text"
                        data-name={'code'}
                        label="کد شش رقمی"

                        labelPlacement='inside'

                    />
                </div>


                <Button

                    isLoading={inLoad}
                    disabled={!(formData.code.validate && formData.code.validate)}
                    type="submit"

                    className={`${styles.login_btn_sign_up} ${!(formData.code.validate && formData.code.validate) && styles.login_btn_login_disable}`}>
                    ورود
                </Button>

                <Button
                    onClick={sendCode}
                    isLoading={inLoad}
                    disabled={!getNewCode}
                    className={`${styles.login_btn_sign_up} ${!getNewCode && styles.login_btn_login_disable}`}>


                    {
                        getNewCode ?
                            <span>
                                      ارسال مجدد کد
                            </span>
                            :
                            <span>

                                ارسال مجدد کد :

                                {timer}
                                ثانیه
                        </span>

                    }

                </Button>
            </form>
        </div>

    </>
}