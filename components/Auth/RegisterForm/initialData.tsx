export const initialData = {
    fullName: {
        value: '',
        validate: false,
        error: 'لظفا حداقل ۳ کاراکتر را وارد نمایید .'
    },
    phoneNumber: {
        value: '',
        validate: false,
        error: 'یک شماره تماس مهتبر وارد نمایید .'
    }
}