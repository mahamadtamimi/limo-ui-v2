export function checkFormData(e: any, formData: any) {
    // console.log(e.target)
    var
        persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
        arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
        fixNumbers = function (str : any)
        {
            if(typeof str === 'string')
            {
                for(var i=0; i<10; i++)
                {
                    str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
                }
            }
            return str;
        };


    const phoneNumber = fixNumbers(e.target.value);
    const phoneNumberRex = /^\(?([1-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    const valid = (phoneNumber.match(phoneNumberRex))


    return {
        ...formData,
        phoneNumber: {
            ...formData.phoneNumber,
            value: phoneNumber,
            validate: valid,
        }
    }

}
