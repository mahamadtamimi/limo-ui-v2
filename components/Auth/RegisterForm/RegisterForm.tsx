import React, {useState} from "react";
import AuthLayout from "@/layouts/auth";
import styles from "@/styles/auth.module.scss";

import {Button, Input} from "@nextui-org/react";
import arrowRight from '@/public/arrow-right.svg'

import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {useRouter} from "next/router";
import {Link} from "@nextui-org/link";
import Image from "next/image";
import {initialData} from "@/components/Auth/RegisterForm/initialData";
import {registerSubmit} from "@/api/register";
import {checkFormData} from "@/components/Auth/RegisterForm/CheckFormData";
import {useAuthDispatch, useAuthState} from "@/components/AuthProvider/auth-context";
import {actionTypes} from "@/components/AuthProvider/reducer";


export default function RegisterForm() {
    const userData = useAuthState()
    const dispatch = useAuthDispatch()

    console.log(userData)

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter()
    const [inLoad, setInLoad] = useState(false)
    const init: any = initialData

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [formData, setFormData] = useState(init)

    async function handleSubmit(event: any) {

        event.preventDefault()
        setInLoad(true)
        toast.loading('لطفا منتظر باشید !', {toastId: "registerToastId"})

        const formDataCollection = new FormData(event.currentTarget)
        const name = formDataCollection.get('name')
        const phoneNumber = formDataCollection.get('phoneNumber')

        try {
            const data = await registerSubmit({name: name, phoneNumber: phoneNumber})

            if (data.status == 200) {
                const result = await data.json()

                if (result.success) {
                    toast.dismiss('registerToastId')
                    toast.success(result?.massage)

                    console.log(result.user)
                    dispatch({
                        type: actionTypes.LOGIN_SUCCESS,
                        payload: {
                            token: null,
                            user: result.user
                        }
                    })
                    localStorage.setItem('user', JSON.stringify(result.user));
                    router.push('/auth/otp')
                }
            } else if (data.status == 403) {
                toast.dismiss('registerToastId')
                toast.error('این شماره تماس قبل ثبت نام شده.')
                const updateDate: any = {
                    ...formData,
                    phoneNumber: {
                        ...formData.phoneNumber,
                        validate: false,
                        error: 'این شماره تماس قبل ثبت نام شده.'
                    }
                }


            }
        }catch(e){
            toast.dismiss('registerToastId')
            toast.error('خطای سرور !')

        }

        setInLoad(false)
    }


    // eslint-disable-next-line react-hooks/rules-of-hooks
    function checkForm(e: any) {
        const data = checkFormData(e, formData)
        console.log(e.target.getAttribute('name'))
        switch (e.target.getAttribute('name')) {

            case 'name':

                const nameVal = e.target.value
                const nameValidate = e.target.value.length > 2;

                setFormData({
                    ...formData,
                    fullName: {
                        ...formData.fullName,
                        value: nameVal,
                        validate: nameValidate,
                    }
                })
                break;

            case 'phoneNumber':

                var
                    persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
                    arabicNumbers = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
                    // @ts-ignore
                    fixNumbers = function (str) {
                        if (typeof str === 'string') {
                            for (var i = 0; i < 10; i++) {
                                str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
                            }
                        }
                        return str;
                    };


                const phoneNumber = fixNumbers(e.target.value);
                if (phoneNumber.length > 10) return
                const phoneNumberRex = /^\(?([1-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                const valid = (phoneNumber.match(phoneNumberRex))


                setFormData({
                    ...formData,
                    phoneNumber: {
                        ...formData.phoneNumber,
                        value: phoneNumber,
                        validate: valid,
                    }


                })
                break;
        }


    }


    return (<>
            <Link className={styles.back_link} onClick={() => router.back()}>
                <Image src={arrowRight} alt={''} width={20}/>
                بازگشت
            </Link>
            <div>

                <ToastContainer

                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={true}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="dark"

                />
            </div>
            <p className={styles.login_section_prag}>ثبت نام</p>
            <div className={styles.login_main_sec}>
                <form onSubmit={handleSubmit}>

                    <div className={`${styles.inputSection}`}>
                        <Input
                            name={'name'}
                            value={formData.fullName.value}
                            variant="bordered"
                            isInvalid={!formData.fullName.validate}
                            color={!formData.fullName.validate ? "danger" : "success"}
                            errorMessage={!formData.fullName.validate && formData.fullName.error}

                            onChange={(e) => checkForm(e)}
                            type="text"
                            data-name={'fullName'}
                            label="نام کامل"

                            labelPlacement='inside'

                        />
                    </div>

                    <div className={`${styles.inputSection} text-left`}>
                        <Input
                            name={'phoneNumber'}
                            value={formData.phoneNumber.value}
                            variant="bordered"
                            isInvalid={!formData.phoneNumber.validate}
                            color={!formData.phoneNumber.validate ? "danger" : "success"}
                            errorMessage={!formData.phoneNumber.validate && formData.phoneNumber.error}

                            onChange={(e) => checkForm(e)}
                            type="text"

                            data-name={'phoneNumber'}

                            label="شماره موبایل"
                            labelPlacement='inside'
                            startContent={
                                <span>+98</span>
                            }
                        />

                    </div>


                    <Button

                        isLoading={inLoad}
                        disabled={!(formData.fullName.validate && formData.phoneNumber.validate)}
                        type="submit"
                        href="/auth/register"
                        className={`${styles.login_btn_sign_up} ${!(formData.fullName.validate && formData.phoneNumber.validate) && styles.login_btn_login_disable}`}>
                        ثبت نام
                    </Button>


                </form>
            </div>
        </>
    )
}
