import {Link} from "@nextui-org/link";
import styles from "@/styles/auth.module.scss";
import Image from "next/image";
import arrowRight from "@/public/arrow-right.svg";
import {toast, ToastContainer} from "react-toastify";
import {Button, Input} from "@nextui-org/react";
import React, {useEffect, useState} from "react";
// import {useAuthDispatch, useAuthState} from "@/components/AuthProvider/auth-context";
import {useRouter} from "next/router";
import {initialData} from "./initialData";
import {checkFormData} from "@/components/Auth/RegisterForm/CheckFormData";
import {registerSubmit} from "@/api/register";
// import {actionTypes} from "@/components/AuthProvider/reducer";
import {loginSubmit} from "@/api/login";
import {useDispatch} from "react-redux";


export default function LoginForm() {
    // const userData = useAuthState()
    // const dispatch = useAuthDispatch()

    const dispatch = useDispatch();

    const [user, setUser] = useState()


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter()
    const [inLoad, setInLoad] = useState(false)
    const init: any = {
        phoneNumber: {
            value: '',
            validate: false,
            error: 'یک شماره تماس مهتبر وارد نمایید .'
        }
    }


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [formData, setFormData] = useState(init)


    useEffect(() => {
        // const userStorage = localStorage.getItem('user');
        // if (userStorage) {

        // const usrdj = JSON.parse(userStorage)

        // if (usrdj[0]?.phone_number && usrdj[0].phone_number.length == 10) {
        //     setFormData({
        //         ...formData,
        //         phoneNumber: {
        //             ...formData.phoneNumebr,
        //             value: usrdj[0].phone_number,
        //             validate: true
        //         }
        //
        //     })
        // }


        // }

    }, []);

    function checkForm(e: any) {
        if (e.target.value.length > 10) return
        const data = checkFormData(e, formData)
        setFormData(data)
    }

    async function handleSubmit(event: any) {

        event.preventDefault()
        setInLoad(true)
        toast.loading('لطفا منتظر باشید !', {toastId: "registerToastId"})

        const formDataCollection = new FormData(event.currentTarget)
        const phoneNumber = formDataCollection.get('phoneNumber')


        const data = await loginSubmit({phoneNumber: phoneNumber})

        if (data.status == 200) {
            const result = await data.json()

            console.log(result)
            if (result.success) {
                toast.dismiss('registerToastId')
                toast.success(result?.massage)

                dispatch({
                    type: actionTypes.LOGIN_SUCCESS,
                    payload: {
                        token: null,
                        user: result.user
                    }
                })

                router.push('/auth/otp')
            } else {
                toast.dismiss('registerToastId')
                toast.error('اکانت نامعتبر')
            }
        } else if (data.status == 403) {

        }
        setInLoad(false)
    }

    return (<>
            <Link className={styles.back_link} onClick={() => router.back()}>
                <Image src={arrowRight} alt={''} width={20}/>
                بازگشت
            </Link>
            <p className={styles.login_section_prag}>ورود</p>
            <div className={styles.login_main_sec}>
                <form onSubmit={handleSubmit}>

                    <div className={`${styles.inputSection} text-left`}>
                        <Input
                            name={'phoneNumber'}
                            value={formData.phoneNumber.value}
                            variant="bordered"
                            isInvalid={!formData.phoneNumber.validate}
                            color={!formData.phoneNumber.validate ? "danger" : "success"}
                            errorMessage={!formData.phoneNumber.validate && formData.phoneNumber.error}

                            onChange={(e) => checkForm(e)}
                            type="text"

                            data-name={'phoneNumber'}

                            label="شماره موبایل"
                            labelPlacement='inside'
                            startContent={
                                <span>+98</span>
                            }
                        />

                    </div>


                    <Button

                        isLoading={inLoad}
                        disabled={!(formData.phoneNumber.validate)}
                        type="submit"
                        href="/auth/register"
                        className={`${styles.login_btn_sign_up} ${!(formData.phoneNumber.validate) && styles.login_btn_login_disable}`}>
                        ورود
                    </Button>


                    <Button
                        as={Link}
                        href="/auth/register"
                        className={styles.login_btn_login}>
                        ثبت نام
                    </Button>


                </form>
            </div>
        </>
    )
}