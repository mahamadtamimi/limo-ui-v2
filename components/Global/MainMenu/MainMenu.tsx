import styles from '../../../styles/mainMenu.module.scss';
import Image from "next/image";

import arowDown from '../../../public/arowDown.svg'

import LIMO from '../../../public/LIMO.svg'
import searchNormal from '../../../public/vuesax-linear-search-normal.svg'
import F48097115 from '../../../public/Frame 48097115.svg'

import {Suspense, useEffect, useState} from "react";
import UserAvatar from "@/components/Global/MainMenu/UserAvatar";
import {Link, Button, Spinner} from "@nextui-org/react";
import {redirect} from "next/navigation";
import {useRouter} from "next/router";
import noti from '@/public/noti.svg'
import icon1 from '@/public/vuesax/bulk/crown.svg'
import icon2 from '@/public/vuesax/bulk/menu.svg'
import icon3 from '@/public/vuesax/bulk/profile-circle.svg'
import icon4 from '@/public/vuesax/bulk/messages-3.svg'
import icon5 from '@/public/vuesax/bulk/logout.svg'
import moment from "moment";


export default function MainBar() {
    const router = useRouter()
    const [user, setUser] = useState(false)
    const [token, setToken] = useState()
    const [error, setError] = useState(false)

    const [response, setResponse] = useState()
    const [pending, setPending] = useState(true)
    // const user = useSelctor
    useEffect(() => {

        // @ts-ignore
        const localUser = JSON.parse(localStorage.getItem('user'))
        const token = localStorage.getItem('token')


        if (!localUser || !token) {
            setUser(false)
            return
        }

        // @ts-ignore
        setToken(token)
        setUser(user)
        fetch(`${process.env.API_PATH}/api/v1/me/plan`, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }

        }).then(res => res.json()).then((data) => {
            if (data.error) setError(true)
            setResponse(data)
            setPending(false)
            // console.log(data)


        })


    }, []);




    const UserMenu = () => {
        let date1 = new Date();
        let date2 = new Date(response?.data[0].pivot.expire);

// Calculating the time difference
// of two dates
        let Difference_In_Time =
            date2.getTime() - date1.getTime();

// Calculating the no. of days between
// two dates
        let Difference_In_Days =
            Math.round
            (Difference_In_Time / (1000 * 3600 * 24));


        return <div className={styles.user_menu_main_sec}>
            <ul>
                <li className={styles.user_menu_item}>
                    <Image src={icon1} alt={''}/>
                    {Difference_In_Days} روز اشتراک دارید.
                </li>
                <li className={styles.user_menu_item}>
                    <Image src={icon2} alt={''}/>
                    لیست تماشا
                </li>
                <li className={styles.user_menu_item}>
                    <Image src={icon3} alt={''}/>
                    ویرایش اطلاعات
                </li>
                <li className={styles.user_menu_item}>
                    <Image src={icon4} alt={''}/>
                    تیکت‌ها
                </li>
                <li className={styles.user_menu_item}>
                    <Image src={icon5} alt={''}/>
                    خروج از حساب کاربری
                </li>
            </ul>
        </div>
    }


    return <>
        <div className={styles.top_bar}>
            <div className={styles.top_bar_menu}>
                <ul>
                    <li>
                        <Link href='/blog'>
                            وبلاگ
                        </Link>
                    </li>
                    <li>
                        <Link href='/public'>
                            تماس با ما
                        </Link>
                    </li>
                    <li>
                        <Link href='/public'>
                            درباره ما
                        </Link>
                    </li>
                    <li>
                        <Link href='/public'>
                            قوانین و مقررات
                        </Link>
                    </li>
                    <li>
                        <Link href='/public'>
                            سوالات متداول
                        </Link>
                    </li>
                </ul>
            </div>


            <div className={styles.top_bar_btn_section}>

                {
                    pending ?
                        <Spinner color={'warning'}/>
                        :
                        <>
                            {
                                // @ts-ignore
                                !response.data &&
                                <Button as={Link} href={'/auth/dashboard/buy-sub'}
                                        className={`${styles.top_bar_kharid_eshtarak}  btn_2x btn_primary`}>
                                    خرید اشتراک
                                </Button>
                            }

                            {
                                // @ts-ignore
                                !response.success ?
                                    <Button href="/auth/login"
                                            as={Link} className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                                        لاگین

                                    </Button>

                                    :

                                    <>

                                        <div className={styles.top_bar_user_bar_sec}>
                                                   <span className={styles.top_bar_user_bar_avatar}>
                                                       {/*<Image src={userAvatar} alt='' width={30} height={33}/>*/}
                                                   </span>
                                            <span className={styles.top_bar_user_bar_user}>
                                                        کیوان نادری
                                                    </span>
                                            <span className={styles.top_bar_user_bar_user_show_menu}>
                                                        <Image src={arowDown} alt='' width={16} height={16}/>
                                                    </span>


                                            {/*<UserMenu/>*/}

                                        </div>


                                        <span className={styles.notific_sec}>
                                                             <Image src={noti} alt='' width={40} height={40}/>
                                                        </span>

                                    </>

                            }
                        </>


                }


            </div>


        </div>

        <div className={styles.top_main_menu}>
            <div>
                <Link href='/public'>
                    <Image src={LIMO} alt='' width={107} height={30}/>
                </Link>
            </div>

            <ul className={styles.main_menu}>
                <li>
                    <Link href='/public'>
                        صفحه اصلی
                    </Link>

                </li>
                <li>
                    <Link href='/movie'>
                        فیلم ها
                        <Image src={arowDown} alt='' width={16} height={16}/>
                    </Link>

                </li>
                <li>
                    <Link href='/serial'>
                        سریال ها
                        <Image src={arowDown} alt='' width={16} height={16}/>
                    </Link>

                </li>
                <li>
                    <Link href=''>
                        صفحه اصلی
                        <Image src={arowDown} alt='' width={16} height={16}/>
                    </Link>

                </li>
                <li>
                    <Link href=''>
                        صفحه اصلی
                        <Image src={arowDown} alt='' width={16} height={16}/>
                    </Link>

                </li>
            </ul>

            <span className={styles.search_sec}>
                <Image src={searchNormal} alt='' width={16} height={16}/>
                <input type="text"/>


                <Image src={F48097115} alt='' width={32} height={32}/>
            </span>
        </div>
    </>
}