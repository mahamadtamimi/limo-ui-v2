import styles from "@/styles/subsCart.module.scss";
import Image from "next/image";
import tick from "../../public/tick-square.svg";
import React, {useEffect, useState} from "react";
import Link from "next/link";
import {Button} from "@nextui-org/react";
import {toast} from "react-toastify";
import {redirect} from "next/navigation";
import {useRouter} from "next/router";
// import PaymentBtn from "../PaymentBtn/PaymentBtn";

export default function SubCart(props :any) {
    const router = useRouter()
    const [isLoad, setIsLoad] = useState(false)

    const [token, setToken] = useState()
    const [response, setResponse] = useState({
        status: null,
        flow: null
    })

    useEffect(() => {
        // @ts-ignore
        const tokenStorage: string = localStorage.getItem('token')
        // @ts-ignore
        setToken(tokenStorage)


    }, []);

    useEffect(() => {
        if (response.status === 200) {
            // @ts-ignore
            router.push(response.flow)
        }
    }, [response]);

    function buyPlan(e:any) {
        setIsLoad(true)

        fetch(`${process.env.API_PATH}/api/v1/get-payment-link`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({plan: e.target.getAttribute('data-role')})
        }).then(res => {
                if (res.status !== 200) {
                    toast.error('خطایی رخ داده است!')
                    setIsLoad(false)

                }
                return res.json()
            }
        ).then(data => {

                setResponse({
                    ...response,
                    // @ts-ignore
                    status: 200,
                    flow: data.StartPay
                })
            }
        )

    }


    const number = props.data.price
    const formattedNumber = number.toLocaleString("en-US")
    return <div className={styles.sub_cart}>
        <p className={styles.sub_top}>{props.data.expire_day} روزه</p>
        <h3 className={styles.sub_cart_title}>{formattedNumber} تومان</h3>
        <ul className={styles.sub_cart_list}>
            <li>
                <Image src={tick} alt='' width={24} height={24}/>
                <p> پخش آنلاین</p>
            </li>
            <li>
                <Image src={tick} alt='' width={24} height={24}/>
                <p> دسترسی به کلیه سریال‌ها</p>

            </li>
            <li>
                <Image src={tick} alt='' width={24} height={24}/>
                <p>بدون هرگونه سانسور و حذفیات</p>

            </li>
            <li>
                <Image src={tick} alt='' width={24} height={24}/>
                <p>پشتیبانی 24 ساعته</p>
            </li>
        </ul>
        <Button isLoading={isLoad} data-role={props.data.id} onClick={(e) => buyPlan(e)} className={styles.btn_buy}>
            خرید اشتراک
        </Button>


    </div>
}