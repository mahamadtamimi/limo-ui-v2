import styles from "@/styles/GeneralInfo.module.scss";

export default function DashboardHead({title}: {title: string}) {
    return <h2 className={styles.dashboard_title}>{title}</h2>


}