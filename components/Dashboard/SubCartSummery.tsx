import styles from "@/styles/dashboard.module.scss";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import {Button, Spinner} from "@nextui-org/react";
import {Link} from "@nextui-org/link";

export default function SubCartSummery(props: {}) {

    const router = useRouter()
    const [user, setUser] = useState(false)
    const [token, setToken] = useState()
    const [error, setError] = useState(false)

    const [response, setResponse] = useState()
    const [pending, setPending] = useState(true)
    // const user = useSelctor
    useEffect(() => {
// @ts-ignore
        const localUser = JSON.parse(localStorage.getItem('user'))
        const token = localStorage.getItem('token')


        if (!localUser || !token) {
            setUser(false)
            return
        }
        // @ts-ignore
        setToken(token)
        setUser(user)
        fetch(`${process.env.API_PATH}/api/v1/me/plan`, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }

        }).then(res => res.json()).then((data) => {
            if (data.error) setError(true)
            setResponse(data)
            setPending(false)
            // console.log(data)


        })

        console.log(localStorage.getItem('user'));

    }, []);


    console.log();
    return <>
        {pending ? <Spinner color={'warning'}/> :
            // @ts-ignore
            !response.data ?
                <>
                    <div className={styles.main_dashboard_sub_sec}>

                        <div className={styles.main_dashboard_sub_sec__empty}>
                            <p className={`${styles.main_dashboard_sub_prag} text-center`}>اشتراک شما غیرفعال است.</p>
                            <Button      as={Link}
                                         href="/auth/dashboard/buy-sub"
                                         className={styles.login_btn_login}>
                                خرید اشتراک
                            </Button>
                        </div>
                    </div>
                </>
                :
                <>
                    <div className={styles.main_dashboard_sub_sec}>
                        <div className={styles.main_circle_chart_sec}>
                            <span className={styles.main_circle_one}></span>
                            <span className={styles.main_circle_two}></span>
                            <span className={styles.main_circle_tree}></span>
                            <span className={styles.main_circle_four}></span>
                        </div>
                        <div>
                            <p className={styles.main_dashboard_sub_prag}>
                                اشتراک

                                <span className={styles.sub_name}>{
                                    // @ts-ignore
                                    response.data.name}</span>
                                طلایی برای شما فعال است .
                            </p>
                            <span className={styles.main_dashboard_sub_sub_prag}>تاریخ انقضا:
                        ۱۴۰۳/۰۵/۲۷
                    </span>
                        </div>
                    </div>
                </>
        }


    </>
}