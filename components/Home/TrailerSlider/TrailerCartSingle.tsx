import styles from '@/styles/HomeComponent.module.scss'
import Image from "next/image";
import Link from "next/link";

export default function TrailerCartSingle(props: any) {

    return <div className={styles.trailer_cart}>
        <div className={styles.slider_cart_image}>
            <Link href={props.data.slug}>


                <Image className={styles.slider_cart_image} src={props.data.image} alt='' width={470}
                       height={240}/>
                <span className={styles.trailer_cart_play_btn}>
                                <Image src='./vuesax-bulk-play-circle.svg' width={56} height={56} alt=''/>
                            </span>


                <div className={styles.trailer_cart_info}>
                    <h2 className={styles.slider_cart_name}>
                        {props.data.title}
                    </h2>
                    <button className={`${styles.slider_cart_btn} btn_primary`}>
                        تماشا
                    </button>
                </div>
            </Link>
        </div>
    </div>
}