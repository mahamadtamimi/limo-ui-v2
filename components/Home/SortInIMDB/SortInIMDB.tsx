import {useEffect, useState} from "react";
import DynamicSliderCart from "@/components/Home/global/DynamicSliderCart";
import DynamicSliderHead from "@/components/Home/global/DynamicSliderHead";


export default function SortInIMDB() {


    const [data, setData] = useState()
    const [pending, setPending] = useState(true)

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/imdb-sort`).then(res => res.json()).then(data => {
            setData(data)
            setPending(false)
        });
    }, []);


    // @ts-ignore
    return <>
        {!pending &&
            // @ts-ignore
            data.length > 0 && <DynamicSliderCart data={data}>
                <DynamicSliderHead title='بر اساس IMDB' subtitle='بر اساس IMDB'/>
            </DynamicSliderCart>

        }

    </>

}