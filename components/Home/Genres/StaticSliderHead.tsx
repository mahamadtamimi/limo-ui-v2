import styles from '@/styles/HomeComponent.module.scss'
export default function StaticSliderHead(props:any){
    return <div className={styles.country_head}>
        <h3 className={styles.country_main_title}>
            {props.title}
        </h3>
        <span className={styles.country_main_subtitle}>
                             {props.subtitle}
                     </span>
    </div>
}