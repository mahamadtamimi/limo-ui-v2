
import Image from "next/image";
import dram from '@/public/dram.jpeg'
import Link from "next/link";
import styles from '@/styles/HomeComponent.module.scss'
export default function GenreCart(props : any) {
    return <div className={styles.genre_cart}>
        <div className={styles.genre_cart_box}>
            <Link href=''>
                <Image className={styles.genre_cart_image} src={dram} alt='' width={280} height={100}/>
                <p className={styles.genre_cart_text}>
                    {props.data.farsi_name}
                </p>
            </Link>

        </div>
    </div>
}