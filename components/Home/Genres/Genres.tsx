import {useEffect, useState} from "react";
import DynamicSliderCart from "@/components/Home/global/DynamicSliderCart";
import DynamicSliderHead from "@/components/Home/global/DynamicSliderHead";
import GenreCart from "@/components/Home/Genres/GenreCart";
import styles from '@/styles/HomeComponent.module.scss'
import StaticSliderHead from "@/components/Home/Genres/StaticSliderHead";

export default function Genres() {

    const [data, setData] = useState()
    const [pending, setPending] = useState(true)

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/latest-movies`).then(res => res.json()).then(data => {
            setData(data)
            setPending(false)
        });
    }, []);


    return <>
        {!pending &&
            // @ts-ignore
            data.length > 0 && <div className={styles.country_sec}>

                <StaticSliderHead title='ژانرها' subtitle='تماشای فیلم براساس  ژانرها دلخواه شما'/>

                <div className={styles.country_grid}>
                    {// @ts-ignore
                        data.map((item) => (<GenreCart key={item.id} data={item}/>))}
                </div>
            </div>

        }


    </>

}