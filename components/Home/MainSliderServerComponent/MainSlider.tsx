'use client'
import React, {useEffect, useRef, useState} from "react";
import {Swiper, SwiperSlide} from "swiper/react";
import 'swiper/css';


import {Navigation, Pagination} from 'swiper/modules';
import MainSliderSlide from "./MainSliderSlide";



// @ts-ignore
export default function MainSlider(props) {
    const [pending, setPending] = useState(true)
    const [data, setData] = useState()
    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/sliders`).then(res => res.json()).then(data => {
            setPending(false);
            setData(data);
        })

    }, []);


    return <> {
        !pending && <Swiper
            navigation={true} modules={[Navigation, Pagination]}
            pagination={{
                clickable: true,
            }}
            className="mySwiper mainSlider">
            {// @ts-ignore
                data.map((item) => (
                    <SwiperSlide key={`mainSlider-${item.media.id}`}>
                        <MainSliderSlide data={item.media}/>
                    </SwiperSlide>

                ))
            }
        </Swiper>
    }

    </>
}