import styles from "@/styles/HomeComponent.module.scss";

// @ts-ignore
export default function DynamicSliderHead({title, subtitle}) {
    return <div className={styles.slider_head}>
        <h3 className={styles.slider_main_title}>
            {title}
        </h3>
        <span className={styles.slider_main_subtitle}>
            {subtitle}
        </span>
    </div>
}
