import styles from '@/styles/HomeComponent.module.scss'
import Image from "next/image";
import dd from "@/public/Wishlist Delete.svg"
import de from "@/public/Wishlist du.svg"
import Link from "next/link";
import logo from '@/public/LIMO.svg'
// @ts-ignore
export default function MovieCart(props) {

    const slug = props.data.serial ? 'serial' : 'movie'

    return <div className={styles.slider_cart}>
        <Link href={`/${slug}/${props.data.slug}`}>
            <div className={styles.slider_cart_image}>
                <ul className={styles.slider_cart_info}>
                    {
                        props.data.subtitle ? <li>
                            <Image src={dd} alt='' width={32} height={32}/>
                        </li> : ''
                    }
                    {
                        props.data.dubbed ?
                            <li>
                                <Image src={de} alt='' width={32} height={32}/>
                            </li> : ''
                    }
                </ul>
                { props.data.cover ?
                    <Image className={styles.slider_cart_image} src={`http://127.0.0.1:8000/storage/${props.data.cover}`} alt=''
                           width={180}
                           height={350}/>:
                    <Image src={logo} alt='' className={styles.margin_padd}   width={180}
                           height={350} />

                }

                <span className={styles.slider_cart_imdb}>
                                imdb : {props.data.imdb_rate}
                </span>
            </div>


            <h2 className={styles.slider_cart_name}>
                {props.data.name}
            </h2>
        </Link>

    </div>
}