import styles from "@/styles/HomeComponent.module.scss";
import Image from "next/image";
import Link from "next/link";

export default function CountryCart(props :any) {

    return <div className={styles.cart}>
        <div className={styles.cart_box}>
            <Link href={`/tags/${props.data.slug}`}>
                <Image className={styles.cart_box_image} src={`${process.env.API_PATH}/storage/${props.data.image}`}
                       alt='' width={60} height={60}/>
                <p>
                    {props.data.name}
                </p>
            </Link>
        </div>
    </div>
}