import styles from '@/styles/HomeComponent.module.scss'
import Image from "next/image";
import image from '@/public/Rectangle 6.png'
export default function Banner() {
    return <div className={styles.banner_sec}>
        <div className={styles.banner_text}>
            <div>
                <h3 className={styles.banner_title}>
                    لذت تماشای فیلم آنلاین
                </h3>
                <p  className={styles.banner_pragh}>
                    با خرید اشتراک از لیمو مووی میتوانید هر زمان با هر دستگاهی بصورت از تماشای فیلم لذت ببرید.
                </p>

            </div>
            <button className=' btn_2x btn_primary'>
                خرید اشتراک
            </button>
        </div>
        <div className={styles.banner_image}>
            <Image src={image} alt='' width={610} height={300}/>
        </div>
    </div>
}