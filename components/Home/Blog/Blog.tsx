// import styles from "./page.module.scss";
import Link from "next/link";
import Image from "next/image";

import React, {useEffect, useState} from "react";
import styles from "@/styles/blog.module.scss";
import StaticSliderHead from "@/components/Home/Genres/StaticSliderHead";
import GenreCart from "@/components/Home/Genres/GenreCart";
// import BlogCart from "../BlogCart/BlogCart";
// import StaticSliderHead from "../Homepage/StaticSliderHead/StaticSliderHead";

// import getBlog from "../../api/getBlog";

import leftArow from "@/public/Movir-vuesax-linear-vuesax-linear-arrow-left.svg";
import BlogCart from "@/components/Home/Blog/BlogCart";

export default function Blog() {

    const [data, setData] = useState()
    const [pending, setPending] = useState(true)

    useEffect(() => {

        // fetch(`${process.env.BLOG_PATH}/index.php/wp-json/api/v1/blog`).then(res => res.json())
        fetch(`${process.env.BLOG_PATH}/index.php/wp-json/api/v1/blog`).then(res => res.json())
            .then(data => {
                // if (data.status == 200) {
                    setData(data)
                    setPending(false)
                // }

            });
    }, []);


    console.log(data)

    // @ts-ignore
    return <>
        {!pending &&
            // @ts-ignore
            data.length > 0 && <div className={styles.blog_box}>
                <div className={styles.blog_box_head}>
                    <StaticSliderHead title='آخرین مطالب' subtitle='آخرین اخبار و مطالب سینمای ایران و جهان'/>
                    <span className={styles.blog_head_link}>
                <Link href='/'>
                    مطالب بیشتر
                    <Image src={leftArow} alt=''/>
                </Link>
            </span>
                </div>
                <div className={styles.blog_sec}>

                    {        // @ts-ignore
                        data.map(item =>
                            <BlogCart key={`blog-${item.id}`} data={item}/>
                        )}

                </div>
            </div>


        }
    </>


}