import styles from "@/styles/blog.module.scss";
import Image from "next/image";
import image from "@/public/Rectangle 6.png";
import df from "@/public/Movir-vuesax-linear-calendar.svg";
import Link from "next/link";
import leftArow from "@/public/Movir-vuesax-linear-vuesax-linear-arrow-left.svg"

export default function BlogCart(props: any) {
    return <div className={styles.blog_cart}>
        <Image className={styles.blog_cart_image} src={image} alt='' width={353} height={203}/>
        <h3 className={styles.blog_cart_title}>
            {props.data.title}
        </h3>
        <p className={styles.blog_cart_prag}>
            {props.data.expert}
        </p>
        <div className={styles.blog_cart_info}>
             <span className={styles.blog_cart_date}>
                        <Image src={df} alt='' height={20} width={20}/>

               <span>
                   {props.data.date}
               </span>
            </span>
            <span className={styles.blog_cart_link}>
                <Link href='/'>
                    مطالب بیشتر
                    <Image src={leftArow} alt=''/>
                </Link>
            </span>
        </div>
    </div>
}