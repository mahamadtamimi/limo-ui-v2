import DynamicSliderCart from "@/components/Home/global/DynamicSliderCart";
import DynamicSliderHead from "@/components/Home/global/DynamicSliderHead";
import {useEffect, useState} from "react";


export default function NewMovies() {


    const [data, setData] = useState()
    const [pending, setPending] = useState(true)

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/latest-movies`).then(res => res.json()).then(data => {
            setData(data)
            setPending(false)
        });
    }, []);


    // @ts-ignore
    return <>
        {!pending &&
            // @ts-ignore
            data.length > 0 && <DynamicSliderCart data={data}>
                <DynamicSliderHead title='جدیدترین فیلم ها' subtitle='جدیدترین فیلم ها'/>
            </DynamicSliderCart>

        }

    </>

}