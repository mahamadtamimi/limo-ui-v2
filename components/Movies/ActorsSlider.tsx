'use client'
import React, {useRef, useState} from "react";
import {Swiper, SwiperSlide} from "swiper/react";
import 'swiper/css';


import {Navigation, Pagination} from 'swiper/modules';

import styles from "@/styles/movie.module.scss";
import ActorCart from "@/components/Movies/ActorCart";


export default function ActorsSlider(props :any) {
    const listItems = props.data.map((item : any) =>
        <SwiperSlide key={`actor-${item.id}`}>
            <ActorCart data={item}/>
        </SwiperSlide>
    );
    return <div className={styles.slider_cart_sec}>
        <Swiper
            slidesPerView={6}
            spaceBetween={24}
            navigation={true}
            modules={[Pagination, Navigation]}
            pagination={{
                clickable: true,
            }}
            className="mySwiper cartSlider">

            {props.children}

            {listItems}

        </Swiper>

    </div>

}