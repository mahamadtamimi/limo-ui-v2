import Image from "next/image";

import styles from '@/styles/movie.module.scss'
import folderOpen from '@/public/Movir-vuesax-linear-folder-open.svg'
import monitor from '@/public/Movir-vuesax-linear-monitor.svg'
import profile2user from '@/public/Movir-vuesax-linear-profile-2user.svg'
import profile from '@/public/Movir-vuesax-linear-profile.svg'
import clock from '@/public/Movir-vuesax-linear-clock.svg'
import rotRight from '@/public/Movir-vuesax-linear-Age.svg'
import global from '@/public/Movir-vuesax-linear-global.svg'
import like from '@/public/Movir-Wishlist.svg'
import Link from "next/link";
import text from '@/public/Movir-vuesax-linear-document-text.svg'
import {spans} from "next/dist/build/webpack/plugins/profiling-plugin";

import dlBox from '@/public/Movir-vuesax-linear-import.svg'

import ardw from '@/public/Movir-vuesax-linear-arrow-up.svg'
// import MostVisited from "../MostVisited/MostVisited";
import React, {useEffect, useState} from "react";
import Breadcrumb from "@/components/Global/Breadcrumb";
import DynamicSliderCart from "@/components/Home/global/DynamicSliderCart";
import DynamicSliderHead from "@/components/Home/global/DynamicSliderHead";
import Banner from "@/components/Home/Banner/Banner";
import ActorsSlider from "@/components/Movies/ActorsSlider";
import DownLoadBox from "@/components/Movies/DownLoadBox";
import Blog from "../Home/Blog/Blog";
import Comments from "@/components/Comments/Comments";


// @ts-ignore
export default function MediaInnerMainHead(props) {

    console.log(props)

    const name = props.data.info.name
    const pathSlug = props.data.info.serial ? '/serial' : '/movie' + `/${props.data.info.slug}`

    const breadCrumb = [
        ['سریال ها', 'serials'],
        [name, pathSlug],
    ]


    const [data, setData] = useState()
    const [pending, setPending] = useState(true)


    return <>
        <div className={styles.main_cover_sec}>
            {
                props.data.info.second_cover &&
                <div className={styles.main_cover}>
                    <Image className={styles.main_cover_image}
                           src={`${process.env.API_PATH}/storage/${props.data.info.second_cover}`}
                           width={1500}
                           height={500} alt=''/>
                </div>
            }

            <Breadcrumb data={breadCrumb}/>

            <div className={styles.main_cover_info}>

                <div className={styles.main_cover_info_grid}>
                    <div className={styles.main_image}>
                        <div className={styles.main_image_position}>
                            <Image src={`${process.env.API_PATH}/storage/${props.data.info.cover}`} width={300}
                                   height={500}
                                   alt=''/>
                            <div className={styles.main_image_btn}>
                                <button className='btn_primary btn_2x'>تماشای آنلاین</button>
                                <button className='btn_gray btn_2x'>تریلر فیلم</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div className={styles.main_text_info_grid}>
                    <div className={styles.main_text_info_grid_head}>
                        <h1 className={styles.main_title}>
                            دانلود فیلم {props.data.info.name}
                        </h1>
                        <Image className='like-btn' src={like} alt=''/>
                    </div>
                    <div className={styles.main_text_info_grid_body}>
                        <div className={styles.main_text_info_info_sec}>
                            {props.data.info.genres.length > 0 &&
                                <p className={styles.main_info}>
                                    <Image src={folderOpen} alt=''/>
                                    ژانر :
                                    {props.data.info.genres.map((item :any) => (

                                        <span key={`genres-${item.id}`}>
                                        <Link href={`/${item.slug}`}>
                                             {item.farsi_name}
                                        </Link>
                                    </span>
                                    ))}
                                </p>
                            }
                            <p className={styles.main_info}>
                                <Image src={monitor} alt=''/>
                                کیفیت :
                                1080p HDTS
                            </p>
                            {props.data.start.length > 0 &&
                                <p className={styles.main_info}>
                                    <Image src={profile2user} alt=''/>
                                    ستارگان:
                                    {
                                        props.data.start.map((item :any) => (

                                            <span key={`star-${item.id}`}>
                                                 <Link href={`/${item.slug}`}>
                                                     {item.full_name} ,
                                                </Link>
                                            </span>
                                        ))
                                    }
                                </p>
                            }
                            {props.data.director.length > 0 &&
                                <p className={styles.main_info}>
                                    <Image src={profile2user} alt=''/>
                                    کارگردان:
                                    {props.data.director.map((item : any) => (


                                        <span key={`director-${item.id}`}>
                                        <Link href={`/${item.slug}`}>
                                             {item.full_name} ,
                                        </Link>
                                    </span>
                                    ))}
                                </p>
                            }
                            {props.data.author.length > 0 &&
                                <p className={styles.main_info}>
                                    <Image src={profile} alt=''/>
                                    نویسنده:
                                    {
                                        props.data.author.map((item : any) => (

                                            <span key={`author-${item.id}`}>
                                        <Link href={`/${item.slug}`}>
                                             {item.full_name} ,
                                        </Link>
                                    </span>
                                        ))
                                    }
                                </p>
                            }
                            <p className={styles.main_info}>
                                <Image src={clock} alt=''/>
                                زمان:
                                {props.data.info.duration}
                            </p>
                            <p className={styles.main_info}>
                                <Image src={rotRight} alt=''/>
                                رده سنی:
                                {props.data.info.age_certification}
                            </p>


                        </div>
                        <div className={styles.main_text_info_slide_sec}>
                            <p className={styles.main_info}>
                                <Image src={text} alt=''/>
                                {props.data.info.summery}
                            </p>
                            <div className={styles.info_box}>
                                <p>hello</p>
                                <p>hello</p>
                                <p>hello</p>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>









        <div className={styles.main_description}>
            <p>{props.data.info.description}</p>
        </div>


        <DownLoadBox data={props.data.info} source={props.data.sources} />


        {
            props.data.actors.length > 0 &&
            <ActorsSlider data={props.data.actors}>
                <DynamicSliderHead title='بازیگران'
                                   subtitle={`بازیگرانی که در فیلم ${props.data.info.farsi_name} باز کرده‌اند`}/>
            </ActorsSlider>
        }
        <Banner/>
        {
            props.data.related > 0 &&
            <DynamicSliderCart data={props.data.related}>
                <DynamicSliderHead title='فیلم های مشابه' subtitle='فیلم های مشابه'/>
            </DynamicSliderCart>
        }


        <Blog/>

        <Comments/>
    </>
}