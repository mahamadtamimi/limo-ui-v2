import styles from "@/styles/movie.module.scss";
import Image from "next/image";
import dlBox from "@/public/Movir-vuesax-linear-import.svg";
import ardw from "@/public/Movir-vuesax-linear-arrow-up.svg";
import Link from "next/link";
import React, {useEffect, useState} from "react";
import {Button} from "@nextui-org/react";

export default function DownLoadBox(props: any) {

    const [user, setUser] = useState()
    useEffect(() => {
        const userStorage = localStorage.getItem('user')
        const tokenStorage = localStorage.getItem('token')

        fetch(`${process.env.API_PATH}/api/me`, {
            headers: {
                authorization: `Bearer ${tokenStorage}`,
            }
        }).then(res => res.json()).then(data => {
            if (data.error) {

            } else {
                setUser(data)
            }
            // console.log(data)
            // setData(data)
            // setPending(false)
            // if (data.success) {
            //     setUser(data)
            // }

        });
    }, []);


    const Btn = () => {
        if (!user) {
            return <Button href="/auth/login"
                           as={Link}
                           className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                لاگین
            </Button>
        }
        if (!user.plan.length) {
            return <Button href="/auth/login"
                           as={Link}
                           className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                خرید پلن
            </Button>
        }

        if (user.plan.length) {
            return <Button href="/auth/login"
                           as={Link}
                           className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                دانلود
            </Button>
        }
    }




    return <div className={styles.download_box}>
        <div className={styles.download_box_head}>
            <Image src={dlBox} alt='' width={40} height={40}/>
            <h3 className={styles.download_box_head_title}>
                لینک‌های دانلود
            </h3>

        </div>


        {props.data.main_lang &&
            <div className={styles.download_box_part}>

                <div className={styles.download_box_part_head}>
                    <h4 className={styles.download_box_part_head_title}>
                        زبان اصلی
                    </h4>

                    <Image src={ardw} alt='' width={24} height={24}/>

                </div>
                {typeof props.source.subtitle !== 'undefined' &&
                    props.source.subtitle.map((item: any) => (

                        <div key={`subtitle-${item.id}`} className={styles.download_box_part_body}>
                            <div className={styles.download_box_part_main_btn_sec}>
                                <Btn/>


                            </div>
                            <div className={styles.download_box_part_info}>
                                <span>
                                         زبان اصلی
                                </span>
                                <p className={styles.download_box_part_info_large}>{item.quality}</p>
                                <div>
                                    <p className={styles.download_box_part_info_small}>
                                        size : {item.size} MB
                                    </p>
                                </div>
                            </div>

                        </div>
                    ))
                }


            </div>
        }
        {props.data.dubbed &&


            <div className={styles.download_box_part}>
                <div className={styles.download_box_part_head}>
                    <h4 className={styles.download_box_part_head_title}>
                        نسخه دوبله فارسی
                    </h4>

                    <Image src={ardw} alt='' width={24} height={24}/>

                </div>
                {typeof props.source.dubbed !== 'undefined' &&
                    props.source.dubbed.map((item: any) => (

                        <div key={`dubbed-${item.id}`} className={styles.download_box_part_body}>
                            <div className={styles.download_box_part_main_btn_sec}>
                                <Btn/>
                            </div>
                            <div className={styles.download_box_part_info}>
                                <span>
                                        نسخه دوبله فارسی
                                </span>

                                <p className={styles.download_box_part_info_large}>{item.quality}</p>
                                <div>
                                    <p className={styles.download_box_part_info_small}>
                                        size : {item.size} MB
                                    </p>

                                </div>
                            </div>

                        </div>
                    ))
                }


            </div>
        }
        {props.data.subtitle &&
            <div className={styles.download_box_part}>

                <div className={styles.download_box_part_head}>
                    <h4 className={styles.download_box_part_head_title}>
                        نسخه زیرنویس فارسی چسبیده
                    </h4>

                    <Image src={ardw} alt='' width={24} height={24}/>

                </div>
                {typeof props.source.subtitle !== 'undefined' &&
                    props.source.subtitle.map((item: any) => (

                        <div key={`subtitle-${item.id}`} className={styles.download_box_part_body}>
                            <div className={styles.download_box_part_main_btn_sec}>
                                <Btn/>

                            </div>
                            <div className={styles.download_box_part_info}>
                                <p>
                                    نسخه زیرنویس فارسی چسبیده
                                </p>
                                <p className={styles.download_box_part_info_large}>{item.quality}</p>
                                <div>
                                    <p className={styles.download_box_part_info_small}>
                                        size : {item.size} MB
                                    </p>

                                </div>
                            </div>

                        </div>
                    ))
                }


            </div>
        }


    </div>

}